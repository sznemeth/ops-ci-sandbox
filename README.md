# ops-ci-sandbox

### DO NOT COMMIT INTO THIS REPO. THE INTENDED USE IS TO FORK THIS REPO INSTEAD OF MAKING MODIFICATIONS ON THE BASE.

Repository contains source files to create a disposable, Docker based CI sandbox environment with Jenkins.

Two docker-compose file for the two different platform was a necessity:
- WinX platform has the Docker API server exposed on tcp/2375 on demand on localhost
- OSX platform does not have Docker API exposed, socat proxy is being used for accessing it on the localhost

#### Usage

Run the following from the root of the git repository.

<pre>
$ docker-compose -f docker-compose-osx.yml start
Starting socat   ... done
Starting jenkins ... done
</pre>

To see running service information in a wide format:

<pre>
$ docker-compose -f docker-compose-osx.yml ps   
     Name                    Command               State                             Ports
--------------------------------------------------------------------------------------------------------------------
jenkins_sandbox   /sbin/tini -- /usr/local/b ...   Up      0.0.0.0:5000->5000/tcp, 50000/tcp, 0.0.0.0:8080->8080/tcp
socat_sandbox     socat TCP-LISTEN:2375,reus ...   Up      127.0.0.1:2375->2375/tcp
</pre>

To stop and dispose your environment.

<pre>
$ docker-compose -f docker-compose-osx.yml stop
Stopping artifactory_sandbox ... done
Stopping jenkins_sandbox     ... done
</pre>

#### To-Do
- [ ] Cleanup script for data volumes
- [ ] Backup / restore functionality
